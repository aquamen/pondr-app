import React, { Component } from 'react';
import { Navigation } from "react-native-navigation";
import { View, Text, Button } from 'react-native';
import Home from "./Home"

Navigation.registerComponent(`navigation.Home`, () => Home);

export class NextScreen extends Component {
    callRestApi(){
      // Github fetch library : https://github.com/github/fetch
      // Call the API page
      fetch('http://10.0.2.2:8080/greeting')
      .then((result) => {
        // Get the result
        // If we want text, call result.text()
        return result.json();
      }).then((jsonResult) => {
        // Do something with the result
        alert(JSON.stringify(jsonResult));
      })
    }

    render() {
      return (
        <View>
          <Text>This is the next screen</Text>
          <Button onPress={this.callRestApi} title="Call REST API"/>
          <Button onPress={goToHomeScreen} title="Back to Home"/>
        </View>
      )
    }
  }
  
export default NextScreen

export const goToHomeScreen = () => Navigation.setRoot({
  root: {
    component: {
      name: "navigation.Home"
    }
  }
});
  

