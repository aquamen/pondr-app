import React, { Component } from 'react';
import { View, Text, Button } from 'react-native';
import NextScreen from "./NextScreen"
import { Navigation } from "react-native-navigation";

Navigation.registerComponent(`navigation.NextScreen`, () => NextScreen);

export class Home extends Component {
    render() {
      return (
        <View>
          <Text>This is the home screen</Text>
          <Button onPress={goToNextScreen} title="Next Screen"/>
        </View>
      )
    }
  }
  
export default Home

export const goToNextScreen = () => Navigation.setRoot({
  root: {
    component: {
      name: "navigation.NextScreen"
    }
  }
});
  