/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import {AppRegistry} from 'react-native';
import { Navigation } from "react-native-navigation";

//components
import App from './App';
//import {name as appName} from './app.json';
import Home from './src/screens/Home';

//AppRegistry.registerComponent(appName, () => App);

AppRegistry.registerComponent('screens.Home', () => Home);
Navigation.registerComponent('navigation.Home', () => Home);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
      root: {
        component: {
          name: "navigation.Home"
        }
      }
    });
});
